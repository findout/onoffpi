# on-off button for Rapberry Pi

![PCB](images/onoff%20pcb%20on%20rpi.jpg){ width=30% }

Onoff is a small add-on to safely turn off a Rapberry Pi with a button.
By connecting a relay to onoff, you may also turn off the power supply for the Rapberry Pi turning it off completely.

With this add-on you dont have to log in to the Pi and give a comand in order to shut the Pi down properly. Instead you can just push a button, and wait for the Pi to complete the shutdown before switching off the power.

# How it works

A simple configuration makes two signals available at GPIO pins:

signal | function
---|---
poweroff | Output, high (3.3V) when the Pi is running, low (0V) when the Pi has shutdown completely.
shutdown | Input, set high (3.3V) to let the Pi run, set low (0V) to make the Pi shut down.

The configuration maps these signals to specific pins. This add-on uses these signals to achive the on-off functionality.

The add-on board uses these signals to make the Pi shutdown, and detect if the shutdown is complete.

# Installation

Before installing the board on your Pi, configure the GPIO pins for the onoff board.

## Edit the config file

For RapiOS Bullseye:

```
sudo nano /boot/config.txt
```

For RapiOS bookworm:

```
sudo nano /boot/firmware/config.txt
```

In both cases go to the botom of the file. After the line [all], append:

```
dtoverlay=gpio-poweroff,gpiopin=17,active_low
dtoverlay=gpio-shutdown,gpio_pin=22
```

Type Ctrl-s and Ctrl-x to save and exit the editor.

## Install the add-on

Turn off the Pi and remove the power connector.
Push the onoff board onto the top-left pins of the GPIO connector, as in the following picture.

![PCB mount](images/onoff%20pcb%20mount.jpg){ width=30% }

## Test it

Connect power to the Pi and wait for it to boot up.Press the button on the onoff board and the Pi should start the shutdown.

# onoff conectors

## SWITCH

Connecting the pins together is the same as pushing the button on the board. It is useful if you want to place the switch away from Pi.

## RELAY

This output is high (3.3V) when the Pi is running and low (0V) when it is safely shutdown. If you connect a relay to this output, you may control the Pi power supply and turn it off when the Pi is shutdown completely.
To turn the Pi on, you must have another way to turn the power supply on.

Either a switch across the relay, that turn the powersupply on while pressing the switch so that the onoff board relay out put keeps the relay on.

Or you may have a switch that supplies the relay through a battery, while the switch is pressed.