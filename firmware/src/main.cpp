#include <Arduino.h>
#include <debounce.h>

// inputs
#define GPIO_POWEROFF 2   // rpi on gpio signal, 0: rpi is off,  1: rpi is on
#define OFFBUTTON 1       // manual shutdown button, 0: pressed, 1: released

// outputs
#define RELAYON 3         // mains power relay, 0: off,  1: on
#define GPIO_SHUTDOWN 4   // rpi gpio signal to start shutdown, 0: start shutdown, 1: let run

enum State {
  S_BOOTING,      // rpi powered, GPIO_POWEROFF not yet high
  S_SOON_BOOTED,  // will be BOOTED after delay
  S_BOOTED,       // rpi powered, GPIO_POWEROFF high
  S_SHUTTINGDOWN, // off-button pressed, GPIO_POWEROFF not yet low
  S_SOON_SHUTDOWN,
  S_SHUTDOWN
};

Debounce offButton(OFFBUTTON, 100);  // debounce button
Debounce gpioPoweroff(GPIO_POWEROFF, 100);  // debounce rpi signal
uint32_t next_time = 0;

// initial state
int state = S_BOOTING;


void setup() {
  pinMode(GPIO_POWEROFF, INPUT);
  pinMode(OFFBUTTON, INPUT_PULLUP);
  pinMode(RELAYON, OUTPUT);
  pinMode(GPIO_SHUTDOWN, OUTPUT);
  digitalWrite(GPIO_SHUTDOWN, 1);
  digitalWrite(RELAYON, 1);
}

void loop() {
  offButton.loop();
  gpioPoweroff.loop();

  switch (state) {
    case S_BOOTING:
      if (gpioPoweroff.getState() == 1) {
        next_time = millis() + 1000;
        state = S_SOON_BOOTED;
      }
      break;
    case S_SOON_BOOTED:
      if (millis() > next_time) {
        state = S_BOOTED;
      }
      break;
    case S_BOOTED:
      if (gpioPoweroff.getState() == 0) {
        next_time = millis() + 1000;
        state = S_SOON_SHUTDOWN;
      }
      if (offButton.getState() == 0) {
        digitalWrite(GPIO_SHUTDOWN, 0);
        state = S_SHUTTINGDOWN;
      }
      break;
    case S_SHUTTINGDOWN:
      if (gpioPoweroff.getState() == 0) {
        next_time = millis() + 1000;
        state = S_SOON_SHUTDOWN;
      }
      break;
    case S_SOON_SHUTDOWN:
      if (millis() > next_time) {
        digitalWrite(RELAYON, 0);
        state = S_SHUTDOWN;
      }
      break;
    case S_SHUTDOWN:
      // stay here until this MCU looses power
      break;
  }
}