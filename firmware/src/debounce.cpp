#include "debounce.h"

  Debounce::Debounce(int aPin, uint32_t time = 100) {
    pin = aPin;
    debounceTime = time;
  }

void Debounce::loop() {
  int newState = digitalRead(pin);
  uint32_t currentTime = millis();
  if (prevState == newState) {
    if (currentTime > debounceReadyTime) {
      state = newState;
    }
  } else {
    // pin state changed - set timer
    prevState = newState;
    debounceReadyTime = currentTime + debounceTime;
  }
}

byte Debounce::getState() {
  return state;
}

void Debounce::setDebounceTime(uint32_t theDebounceTime) {
  debounceTime = theDebounceTime;
}