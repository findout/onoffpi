#include <Arduino.h>

/*
debounce input pins for switches and other signals.
When in signal is stable more than debounce'time after
a change, state changes.

*/
class Debounce {
private:
  byte pin;
  uint32_t debounceTime; // mS
  byte state; // debounced state

  byte prevState;  // previous read pin state during bouncing
  uint32_t debounceReadyTime;

public:
  /**
   * Creates a new debounced pin, with optional specified debounce time.
  */
  Debounce(int aPin, uint32_t time = 100);

  /**
   * Call this as often as possible, to get debounced state updated.
  */
  void loop();

  /**
   * Returns debounced pin state.
   * Is set earliest debounceTime mS after a pin state change.
  */
  byte getState();

  /**
   * Sets a new debounce time.
  */
  void setDebounceTime(uint32_t theDebounceTime);
};