# Raspberry PI start and shutdown with power relay

## RPI preps

Add to /boot/config.txt (RaspiOS bullseye) or boot/firmware/config.txt (RaspiOS bookworm):

After [all] section at end of file:

```
dtoverlay=gpio-poweroff,gpiopin=17,active_low
dtoverlay=gpio-shutdown,gpio_pin=22
```

This means:
* pin 17 is high (3.3V) when rpi is on, and become low when rpi is ahut down.
* by pulling pin 22 low, the rpi will shut down gracefully and lower pin 17 when complete.

## States

![states](states.drawio.png)